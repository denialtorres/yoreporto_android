angular.module('yociudadano')

.controller('IntroCtrl', function($scope, $ionicLoading, $state, $ionicSlideBoxDelegate, $ionicNavBarDelegate) {

  $ionicLoading.hide();
  $ionicSlideBoxDelegate.slide(0);
  // Called to navigate to the main app
  $scope.startApp = function() {
    $state.go('tab.map');
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
  };
})