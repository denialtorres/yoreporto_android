angular.module('yociudadano')

.factory("Contacts", function($http, SERVER) {
  var o = {
  	list: []
  };

  o.getContacts = function(session_id) {
  	var req = {
  		method: 'GET',
  		url: SERVER.url + '/api/v1/contacts',
  		headers: {
  			'SessionId': session_id
  		}
  	};

  	return $http(req).success(function(data) {
  		o.list = [];
  		angular.forEach(data, function(value, key) {
  			value.avatar_url = SERVER.url + value.avatar_url;
  			o.list.push(value);
  		});
  	});
  }

  o.init = function(session_id) {
  	if (o.list.length === 0) {
  		return o.getContacts(session_id);
  	}
  }

  o.refresh = function(session_id) {
  	return o.getContacts(session_id);
  }

  return o;
})