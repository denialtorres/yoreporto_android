angular.module('yociudadano')

.factory("Comments", function($http, SERVER) {
	var o = {

	};

	o.like = function(session_id, comment_id) {
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/comments/' + comment_id + '/like',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req);
	}

	o.unlike = function(session_id, comment_id) {
		var req = {
			method: 'DELETE',
			url: SERVER.url + '/api/v1/comments/' + comment_id + '/unlike',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req);
	}

	o.flag = function(session_id, comment_id) {
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/comments/' + comment_id + '/flag',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req);
	}

	return o;
})