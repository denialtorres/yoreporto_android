angular.module('yociudadano')

.factory('VideosService', function(
	$http,
	$ionicLoading,	
	$q, 
	$localstorage, 
	SERVER, 
	CLIENTS, 
	$cordovaOauth,
	$twitterApi,	
	$state, 
	$cordovaFileTransfer,
	$cordovaOauthUtility,
	$ionicPopup
) {
	var o = {
		session_id: false,
		name: false,
		username: false,
		since: false, 
		image: false,
		reports: false,
		comments: false,
		level: false,
		fbAccessToken: false,
		twitterInfo: false,
		fbInfo: false,
		isAFacebookUser: false,
		isATwitterUser: false,
		provider: false
		
	};

	o.getStatistics = function() {
		var req = {
			method: 'GET',
			url: SERVER.url + '/api/v1/user/stats',
			headers: {
				'SessionId': o.session_id
			}
		}
		return $http(req).success(function(data) {
			// merge data
			o.reports = data.user.report_count;
			o.comments = data.user.comment_count;
			o.level = data.user.level;
		});
	}

	o.finishSignup = function(userdata) {
		var params = {
			auth_api: {
				username: userdata.username,
				birthdate: userdata.birthdate,
				gender: userdata.gender,
				email: o.fbInfo.email
			}				
		};

		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/finish_signup',
			headers: {
				'SessionId': o.session_id,
			},
			data: params
		};

		return $http(req).success(function(data) {
			
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level);
		});
	}

//-------------------------------------------------------------
o.finishTwitterSignup = function(userdata) {
				$ionicPopup.alert({
						title: '<b>Ya casi!</b>',
						template: 'Confirma el correo que te acabamos de enviar para completar el registro',
						okText: 'Aceptar',
						okType: 'button-energized'
				})
	
		var params = {
			auth_api: {
				username: userdata.username,
				birthdate: userdata.birthdate,
				gender: userdata.gender,
				email: userdata.email
			}				
		};

		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/finish_signup',
			headers: {
				'SessionId': o.session_id,
			},
			data: params
		};

		return $http(req).success(function(data) {
			
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level);
		});
	
	
	}	
	
//-------------------------------------Bloque de funciones para Facebook----------------------------------------
	o.fbSignIn = function() {
		$ionicLoading.show({
			duration: 30000,
			noBackdrop: true,
			template: '<p> <ion-spinner icon="bubbles" class="spinner-energized "></ion-spinner><br>cargando</p>'
			})
		return $cordovaOauth.facebook(CLIENTS.facebook, ["email"]).then(function(result) {
			o.fbAccessToken = result.access_token;
			////alert(JSON.stringify(result));
		}, function(error) {
			var messagere = /flow/
			if (!messagere.test(error)) {
				////alert("Error -> " + error);
			}
			console.log("Error -> " + error);
		});
	}

	o.fbGetData = function() {
		return $http.get('https://graph.facebook.com/v2.2/me', { params: { access_token: o.fbAccessToken, fields: 'id,name,email', format: 'json' } }).then(function(response) {
			////alert(response.data);
			////alert(response.data.name);
			////alert(response.data.id);
			////alert(response.data.email);
			o.fbInfo = response.data;
			
		});
	}
   //despues de recolectar datos del usuario en fb comparamos ? 
	o.fbAuth = function() {
		o.provider = 'facebook'; 
		var params;
        ////alert("estas dentro de fbAuth");
		params = {
			auth_api: {
				email: o.fbInfo.email,
				name: o.fbInfo.name,
				picture: 'http://graph.facebook.com/' + o.fbInfo.id + '/picture'
			}
		};

	
		 
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/signin',
			headers: {
				'Uid': o.fbInfo.id,
				'Provider': 'facebook'
			},
			data: params
		};

		////alert(req.data);
		return $http(req).success(function(data) {
			
			o.isATwitterUser = true;
			o.isAFacebookUser = true;
			////alert("samanta");
			// $ionicLoading.hide() 
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level);
		});
	}

// Bloque para twitter

		 var twitterKey = 'STORAGE.TWITTER.KEY';
		 var clientId = 'Npc5ideB1SzbwHn7492syZtVo';
		 var clientSecret = 'GkRjASfNwQilZvE373hfUSqQRIrAmU2nV6ZrG7zAMDZfyW8El9';
		 var myToken = '';
		 
		 
	o.twitterSignIn = function(){
			
		$ionicLoading.show({
			duration: 30000,
			noBackdrop: true,
			template: '<p> <ion-spinner icon="bubbles" class="spinner-energized "></ion-spinner><br>cargando</p>'
			})
		return $cordovaOauth.twitter(clientId, clientSecret).then(function (succ) {
        myToken = succ;
        window.localStorage.setItem(twitterKey, JSON.stringify(succ));
        $twitterApi.configure(clientId, clientSecret, succ);

		$twitterApi.getUserData(myToken.screen_name,myToken.user_id);
		
		},function(error) {
    console.log('error');
    console.log(error);
	});
	}

	o.twitterGetData = function() {

	  $twitterApi.getUserData(myToken.screen_name,myToken.user_id);
	  $twitterApi.getData();
	  o.twitterInfo = $twitterApi.getData();
		return true; 
	}
	
	
 //despues de recolectar datos del usuario en twitter comparamos ? 
	o.twitterAuth = function() {
		//alert('dentro de twitterAuth');
		//alert(JSON.stringify($twitterApi.getData()));
		o.provider = 'twitter'; 
		var params;
		params = {
			auth_api: {
				email: 'temporal@temporal.com',
				name: myToken.screen_name,
				picture: 'http://s6315.storage.proboards.com/6306315/i/8QrWd_fJTozBJCaDmcMs.png'
				 
			}
		};
		
	
		 
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/signin',
			headers: {
				'Uid': myToken.user_id,
				'Provider': 'twitter'
			},
		
			data: params
		};
			
			return $http(req).success(function(data) {
			o.isATwitterUser = true;
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level);
		});
	}
	
//--------------------------------------------------------------------------------------------------------------	
	o.auth = function(username, password, signingUp, signinUpData) {
		$ionicLoading.show({
			duration: 30000,
			noBackdrop: true,
			template: '<p> <ion-spinner icon="bubbles" class="spinner-energized "></ion-spinner><br>cargando</p>'
			})
		var authRoute;
		var params;
		if (signinUpData) {
			params = {
				auth_api: {
					email: signinUpData.email,
					name: signinUpData.name,
					username: signinUpData.username,
					password: signinUpData.password,
					gender: signinUpData.gender,
					birthdate: signinUpData.birthdate
				}
			};
		} else {
			params = {};
		}

		if (signingUp) {
			authRoute = 'signup';
		} else {
			authRoute = 'signin';
		}

		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/' + authRoute,
			headers: {
				'Username': username,
				'Password': password
			},
			data: params
		};
		return $http(req).success(function(data) {
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level);
		});
	}

	// Set the session data
	o.setSession = function(session_id, name, username, since, image, reports, comments, level) {
		if (session_id) o.session_id = session_id;
		if (name) o.name = name;
		if (username) o.username = username;
		if (since) o.since = since;
		if (image) {
			var facebookre = /facebook/;
			if (!facebookre.test(image)) {
				//alert('aqui no se');
				
				o.image = $twitterApi.getData();
				//alert(JSON.stringify(o.image));
				//o.image = 'http://pbs.twimg.com/profile_images/546860489511538688/CwvYihoN.jpeg';
				//o.image = JSON.stringify(image);
				//o.image = 'http://38.media.tumblr.com/avatar_ba0353ec855e_128.png';
				//alert(JSON.stringify(o.image)); 
			} else {
				//alert('ni aqui');
				o.image = image;
				//alert(JSON.stringify(o.image)); 
			}
		} else {
			o.image = 'img/missing.png';
		}		
		o.reports = reports;
		o.comments = comments;
		if (level) o.level = level;

		$localstorage.setObject('user', { session_id: session_id, name: name, username: username, since: since, comments: comments, reports: reports, image: image });
	}

	// Check for available session
	o.checkSession = function() {
		var defer = $q.defer();

		if (o.session_id) {
			// if session is already initialized in the service
			defer.resolve(true);
		} else {
			// detect if there is a session in localstorage from previous use
			// if it is, pull it into service
			var user = $localstorage.getObject('user');

			if (user.session_id) {
				// if there is a user, lets get information from server
				o.setSession(user.session_id, user.name, user.username, user.since, user.image, false, false, false)
				var facebookre = /facebook/
				if (facebookre.test(user.image)) {
					o.isAFacebookUser = true;
				}
				o.getStatistics().then(function() {
					defer.resolve(true);
				}, function(reason) {
					o.destroySession();
					defer.resolve(false);
				});
			} else {
				// no user info in localstorage, reject
				defer.resolve(false);
			}
		}
		return defer.promise;
	}

	o.fbUser = function() {
		return o.isAFacebookUser;
	}

	o.destroySession = function() {
		$localstorage.setObject('user', {});
		o.session_id = false;
		o.name = false;
		o.username = false;
		o.since = false;
		o.image = false;
		o.reports = false;
		o.comments =  false;
		o.level = false;
		o.isAFacebookUser = false;
	}

	o.recoverPassword = function(email) {
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/recover',
			data: {
				auth_api: {
					email: email
				}
			}
		};
		return $http(req);
	}

	o.changePicture = function(session_id, newImage) {
		var options = {
			fileKey: 'user_api[image]',
			headers: {
				'SessionId': session_id
			},
			params: {

			}
		};

		var url = SERVER.url + '/api/v1/user/change_picture';

		
		return	$cordovaFileTransfer.upload(url, newImage, options);
	}

	o.setImage = function(newImage) {
		o.image = newImage;
	}

	return o;
});